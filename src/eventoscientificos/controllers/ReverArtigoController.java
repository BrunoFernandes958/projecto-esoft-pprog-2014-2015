/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.ReverArtigo;
import eventoscientificos.model.Submissao;
import java.util.List;

/**
 *
 * @author Bruno
 */
public class ReverArtigoController {

    private Empresa m_empresa;
    private Evento m_evento;
    private Submissao m_submissao;
    private ReverArtigo m_reverArtigo;

    public ReverArtigoController(Empresa empresa) {
        m_empresa = empresa;
    }

    public List<Evento> iniciarRevisao() {
        return this.m_empresa.getListaEventosPodeSubmeter();
    }

    public void selectEvento(Evento e) {
        m_evento = e;
        this.m_reverArtigo = this.m_evento.novaReverArtigo();
    }

    public List<Submissao> getArtigosSubmetidos() {
        return m_evento.getListaSubmissao();
    }

    public void setAvaliacao(String avaliacao) {
        this.m_reverArtigo.setAvaliacao(Integer.parseInt(avaliacao));
    }

    public void setDescricao(String descricao) {
        this.m_reverArtigo.setDescricao(descricao);
    }

    public boolean valida() {
        return true;
    }

}
