/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import eventoscientificos.model.Empresa;
import eventoscientificos.model.Utilizador;

/**
 *
 * @author Bruno
 */
public class AlterarUtilizadorController {

    private Empresa m_empresa;
    private Utilizador m_utilizador;

    public AlterarUtilizadorController(Empresa empresa) {
        m_empresa = empresa;
        m_utilizador = new Utilizador();
    }

    public Utilizador uClone(String strNome, String strUsername, String strPassword, String strEmail) {

        m_utilizador.uClone(strNome, strUsername, strPassword, strEmail);

        return m_utilizador;

    }

    public Utilizador verficarUtilizador(String username) {
        Utilizador utilizador = m_empresa.getUtilizador(username);
        if (utilizador != null) {
            return utilizador;
        } else {
            return null;
        }
    }

    public void alterarDados(Utilizador uRegistado, Utilizador uClone) {
        m_empresa.alterarUtilizador(uRegistado, uClone);
    }
}
