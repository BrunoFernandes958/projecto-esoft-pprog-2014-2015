package eventoscientificos.model;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 *
 * @author Nuno Silva
 */
public class Empresa {

    private List<Utilizador> m_listaUtilizadores;
    private List<Evento> m_listaEventos;
    private Empresa m_empresa;

    public Empresa() {
        m_listaUtilizadores = new ArrayList<Utilizador>();
        m_listaEventos = new ArrayList<Evento>();

        //fillInData();
    }

    public Empresa getListaEmpresa() {
        return m_empresa;
    }

    public Utilizador novoUtilizador() {
        return new Utilizador();
    }

    //Metodos de Utilizador
    public boolean registaUtilizador(Utilizador u) {
        if (u.valida() && validaUtilizador(u)) {
            return addUtilizador(u);
        } else {
            return false;
        }
    }

    private boolean validaUtilizador(Utilizador u) {
        return true;
    }

    public Utilizador getUtilizador(String strUsername) {
        for (Utilizador u : this.m_listaUtilizadores) {
            String s1 = u.getUsername();
            if (s1.equalsIgnoreCase(strUsername)) {
                return u;
            }
        }
        return null;
    }

    private boolean addUtilizador(Utilizador u) {
        return m_listaUtilizadores.add(u);
    }

    public List<Utilizador> getListaUtilizadores() {
        List<Utilizador> ut = new ArrayList<Utilizador>();

        for (ListIterator<Utilizador> it = m_listaUtilizadores.listIterator(); it.hasNext();) {
            ut.add(it.next());
        }
        return ut;
    }

    public void alterarUtilizador(Utilizador uRegistado, Utilizador uClone) {

        for (int i = 0; i < m_listaUtilizadores.size(); i++) {
            Utilizador u = m_listaUtilizadores.get(i);
            if (u == uRegistado) {
                System.out.println(u);
                m_listaUtilizadores.set(i, uClone);
            }
        }
    }

    //Metodos Eventos
    public Evento novoEvento() {
        return new Evento();
    }

    public boolean registaEvento(Evento e) {
        if (e.valida() && validaEvento(e)) {
            return addEvento(e);
        } else {
            return false;
        }
    }

    private boolean validaEvento(Evento e) {
        return true;
    }

    private boolean addEvento(Evento e) {
        return m_listaEventos.add(e);
    }

    public List<Evento> getEventosOrganizador(String strId) {
        List<Evento> leOrganizador = new ArrayList<Evento>();

        Utilizador u = getUtilizador(strId);

        if (u != null) {
            for (Iterator<Evento> it = m_listaEventos.listIterator(); it.hasNext();) {
                Evento e = it.next();
                List<Organizador> lOrg = e.getListaOrganizadores();

                boolean bRet = false;
                for (Organizador org : lOrg) {
                    if (org.getUtilizador().equals(u)) {
                        bRet = true;
                        break;
                    }
                }
                if (bRet) {
                    leOrganizador.add(e);
                }
            }
        }
        return leOrganizador;
    }

    public List<Evento> getListaEventosPodeSubmeter() {
        List<Evento> le = new ArrayList<Evento>();

        for (Evento e : m_listaEventos) {
            if (e.aceitaSubmissoes()) {
                le.add(e);
            }
        }

        return le;
    }

    public List<Evento> getListaEventos() {
        List<Evento> lEven = new ArrayList<Evento>();

        for (ListIterator<Evento> it = m_listaEventos.listIterator(); it.hasNext();) {
            lEven.add(it.next());
        }

        return lEven;
    }

    @Override
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
        }
        if (outroObjeto == null || this.getClass() != outroObjeto.getClass()) {
            return false;
        }
        Empresa outraEmpresa = (Empresa) outroObjeto;
        return m_listaUtilizadores.equals(outraEmpresa.m_listaUtilizadores) && m_listaEventos.equals(outraEmpresa.m_listaEventos);
    }

}
