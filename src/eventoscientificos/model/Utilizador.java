package eventoscientificos.model;

/**
 *
 * @author Nuno Silva
 */
public class Utilizador {

    private String m_strNome;
    private String m_strUsername;
    private String m_strPassword;
    private String m_strEmail;

    public Utilizador() {
    }

    public void setNome(String strNome) {
        this.m_strNome = strNome;
    }

    public String getNome() {
        return m_strNome;
    }

    public void setUsername(String strUsername) {
        m_strUsername = strUsername;
    }

    public String getUsername() {
        return m_strUsername;
    }

    public void setPassword(String strPassword) {
        m_strPassword = strPassword;
    }

    public void setEmail(String strEmail) {
        this.m_strEmail = strEmail;
    }

    public String getEmail() {
        return m_strEmail;
    }

    public void uClone(String strNome,String strUsername,String strPassword,String strEmail){
        m_strNome=strNome;
        m_strUsername=strUsername;
        m_strPassword=strPassword;
        m_strEmail=strEmail;      
        
    }

    
    
    public boolean valida() {
        if(m_strNome.isEmpty()||m_strUsername.isEmpty()||m_strPassword.isEmpty()||m_strEmail.isEmpty()){
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        String str = "Utilizador:\n";
        str += "\tNome: " + this.m_strNome + "\n";
        str += "\tUsername: " + this.m_strUsername + "\n";
        str += "\tPassword: " + this.m_strPassword + "\n";
        str += "\tEmail: " + this.m_strEmail + "\n";

        return str;
    }

}
