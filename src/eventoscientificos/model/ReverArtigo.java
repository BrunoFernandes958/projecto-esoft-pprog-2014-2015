/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

/**
 *
 * @author Bruno
 */
public class ReverArtigo {
    private int avaliacao;
    private String descricao;

    public ReverArtigo() {
    }

    public ReverArtigo(int avaliacao, String descricao) {
        this.avaliacao = avaliacao;
        this.descricao = descricao;
    }

    /**
     * @return the avaliacao
     */
    public int getAvaliacao() {
        return avaliacao;
    }

    /**
     * @return the descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @param avaliacao the avaliacao to set
     */
    public void setAvaliacao(int avaliacao) {
        this.avaliacao = avaliacao;
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public String toString() {
        return "Avaliacacao: "+avaliacao+"\nDescricao: "+descricao;
    }
    
    
}
