package eventoscientificos.ui;

import eventoscientificos.model.Empresa;
import eventoscientificos.model.Utilizador;
import java.io.IOException;
import utils.*;

/**
 *
 * @author Nuno Silva
 */

public class MenuUI
{
    private Empresa m_empresa;
    private String opcao;

    public MenuUI(Empresa empresa)
    {
        m_empresa = empresa;
    }

    public void run() throws IOException
    {
        do
        {
            //opcao = "1";

            System.out.println("1. Registar utilizador");
            System.out.println("2. Criar Evento Cientifico");
            System.out.println("3. Criar Comissao de Programa");
            System.out.println("4. Submeter Artigo");
            System.out.println("5. Rever Artigo Cientifico");
            System.out.println("6. Definir Sessao Tematica");
            System.out.println("7. Criar CP de Sessao Tematica");
            System.out.println("8. Distribuir Revisao de Artigos");
            System.out.println("9. Alterar Utilizador");
            System.out.println("10. Alterar Submissao");
            System.out.println("11. Licitar Artigo");
            System.out.println("0. Sair");

            opcao = Utils.readLineFromConsole("Introduza opcao: ");

            if( opcao.equals("1") )
            {
                RegistarUtilizadorUI uiRU = new RegistarUtilizadorUI(m_empresa);
                uiRU.run();
            }

            if( opcao.equals("2") )
            {
                CriarEventoCientificoUI uiCEC = new CriarEventoCientificoUI(m_empresa);
                uiCEC.run();
            }
            
            if( opcao.equals("3") )
            {
                CriarCPUI uiCCP = new CriarCPUI(m_empresa);
                uiCCP.run();
            }

            if( opcao.equals("4") )
            {
                SubmeterArtigoUI uiSA = new SubmeterArtigoUI(m_empresa);
                uiSA.run();
            }if(opcao.equals("5")){
                ReverArtigoUI uiRe = new ReverArtigoUI(m_empresa);
                uiRe.run();
            }if(opcao.equals("6")){
            }if(opcao.equals("7")){
            }if(opcao.equals("8")){
            }if(opcao.equals("9")){
                AlterarUtilizadorUI uiAU= new AlterarUtilizadorUI(m_empresa);
                uiAU.run();
            }if(opcao.equals("10")){
            }if(opcao.equals("10")){
                
            }
            
             if (opcao.equals("20")) {
                for (Utilizador u : m_empresa.getListaUtilizadores()) {
                    if (u != null) {
                        System.out.println(u);
                    }
                }
            }
        }
        while (!opcao.equals("0") );
    }
}
