/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.controllers.AlterarUtilizadorController;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Utilizador;
import utils.Utils;

/**
 *
 * @author Bruno
 */
public class AlterarUtilizadorUI {

    private Empresa m_empresa;
    private AlterarUtilizadorController m_controllerAU;

    public AlterarUtilizadorUI(Empresa empresa) {
        m_empresa = empresa;
        m_controllerAU = new AlterarUtilizadorController(m_empresa);
    }

    public void run() {
        String nomeUtilizador = Utils.readLineFromConsole("Introduza o Username");
        Utilizador utilRegistado = m_controllerAU.verficarUtilizador(nomeUtilizador);
        if (utilRegistado != null) {
            System.out.println("Dados Antigos:");
            System.out.println("Nome Antigo: " + utilRegistado.getNome());
            System.out.println("Nome Antigo: " + utilRegistado.getUsername());
            System.out.println("Nome Antigo: " + utilRegistado.getEmail());

            Utilizador uClone = alterarDados();
            m_controllerAU.alterarDados(utilRegistado, uClone);
            //apresentaNovosDados(utilizado);
        } else {
            System.out.println("Utilizador nao existente");
        }

    }

    private Utilizador alterarDados() {
        String strNome = Utils.readLineFromConsole("Introduza Nome: ");
        
        String strUsername = Utils.readLineFromConsole("Introduza Username: ");

        String strPassword = Utils.readLineFromConsole("Introduza Password: ");

        String strEmail = Utils.readLineFromConsole("Introduza Email: ");

        return m_controllerAU.uClone(strNome, strUsername, strPassword, strEmail);
    }

    private void apresentaNovosDados(Utilizador utilizador) {
        if (utilizador == null) {
            System.out.println("Utilizador não registado.");
        } else {
            System.out.println(utilizador.toString());
        }
    }
}
