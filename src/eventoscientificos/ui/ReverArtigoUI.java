/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.controllers.ReverArtigoController;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Submissao;
import java.util.List;
import utils.Utils;

/**
 *
 * @author Bruno
 */
public class ReverArtigoUI {

    private Empresa m_empresa;
    private ReverArtigoController m_controllerRever;

    public ReverArtigoUI(Empresa empresa) {
        m_empresa = empresa;
        m_controllerRever = new ReverArtigoController(m_empresa);
    }

    public void run() {

        List<Evento> ev = m_controllerRever.iniciarRevisao();
        apresentaEventos(ev);

        Evento e = selecionaEvento(ev);

        if (e != null) {
            m_controllerRever.selectEvento(e);
            List<Submissao> su = m_controllerRever.getArtigosSubmetidos();
            apresentaArtigos(su);

            Submissao sub = selecionaArtigo(su);

            if (sub != null) {
                String avaliacao = Utils.readLineFromConsole("Qual a avaliacao do artigo?");
                m_controllerRever.setAvaliacao(avaliacao);
                String descricao = Utils.readLineFromConsole("Qual a avaliacao do artigo?");
                m_controllerRever.setDescricao(descricao);

                String strConfirmacao = "Confirma a avaliacao do artigo: \n" + m_controllerRever.toString() + "\n Opção (S/N):";
                boolean bConfima = confirma(strConfirmacao);

                m_controllerRever.valida();
            } else {
                System.out.println("Nao existem artigos");
            }
        } else {
            System.out.println("Nao existem Eventos");
        }

    }

    private Evento selecionaEvento(List<Evento> le) {
        String opcao;
        int nOpcao;
        do {
            opcao = Utils.readLineFromConsole("Introduza opção: ");
            nOpcao = new Integer(opcao);
        } while (nOpcao < 0 || nOpcao > le.size());

        if (nOpcao == 0) {
            return null;
        } else {
            return le.get(nOpcao - 1);
        }
    }

    private void apresentaEventos(List<Evento> le) {
        System.out.println("Eventos: ");

        int index = 0;
        for (Evento e : le) {
            index++;

            System.out.println(index + ". " + e.toString());
        }
        System.out.println("");
        System.out.println("0 - Cancelar");
    }

    private void apresentaArtigos(List<Submissao> su) {
        System.out.println("Sumissoes: ");

        int index = 0;
        for (Submissao s : su) {
            index++;

            System.out.println(index + ". " + s.toString());
        }
        System.out.println("");
        System.out.println("0 - Cancelar");

    }

    private Submissao selecionaArtigo(List<Submissao> su) {
        String opcao;
        int nOpcao;
        do {
            opcao = Utils.readLineFromConsole("Introduza opção: ");
            nOpcao = new Integer(opcao);
        } while (nOpcao < 0 || nOpcao > su.size());

        if (nOpcao == 0) {
            return null;
        } else {
            return su.get(nOpcao - 1);
        }
    }

    private boolean confirma(String questao) {
        String strConfirma;
        do {
            strConfirma = Utils.readLineFromConsole(questao);
        } while (!strConfirma.equalsIgnoreCase("s") && !strConfirma.equalsIgnoreCase("n"));

        return strConfirma.equalsIgnoreCase("s");
    }
}
