/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.



263202+2+.
 */

package eventoscientificos.ui;


import eventoscientificos.model.Empresa;

/**
 *
 * @author Nuno Silva
 */

public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        try
        {
            Empresa empresa = new Empresa();

            MenuUI uiMenu = new MenuUI(empresa);

            uiMenu.run();
        }
        catch( Exception e )
        {
            e.printStackTrace();
        }
    }

}
